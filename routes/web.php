<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::get('login',array('as'=>'login','uses'=>'LoginController@login'));
Route::post('login',array('as'=>'login','uses'=>'LoginController@loginstore'));
Route::get('logout',array('as'=>'logout','uses'=>'LoginController@logout'));

Route::group(['middleware' => 'demo'],function(){
    Route::get('import/{name}',array('as'=>'front','uses'=>'DashboardController@front'));
    Route::get('sales',array('as'=>'sales','uses'=>'ExcelController@sales'));
    Route::post('import/sales',array('as'=>'salesregister','uses'=>'ExcelController@import'));
    Route::get('salesreturn',array('as'=>'salesreturns','uses'=>'SalesreturnController@salesreturn'));
    Route::post('import/salesreturn',array('as'=>'salesreturn','uses'=>'SalesreturnController@import'));

    //Flipcart Sheet Converter
    Route::get('converter/import',array('as'=>'import','uses'=>'ConverterController@view'));
    Route::post('converter/import',array('as'=>'import.post','uses'=>'ConverterController@import'));
});