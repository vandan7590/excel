<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateFlipcartsheetsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('flipcartsheets', function (Blueprint $table) {
            $table->increments('id');
            $table->string('BillNo')->nullable();
            $table->string('BillDate')->nullable();
            $table->string('BillType')->nullable();
            $table->string('PartyName')->nullable();
            $table->string('InvoiceType')->nullable();
            $table->string('BillAmount')->nullable();
            $table->string('City')->nullable();
            $table->string('state')->nullable();
            $table->string('BillToBillRequired')->nullable();
            $table->string('BookCode')->nullable();
            $table->string('Narration')->nullable();
            $table->string('ProductName')->nullable();
            $table->string('Quantity')->nullable();
            $table->string('Unit')->nullable();
            $table->string('Quantity2')->nullable();
            $table->string('Unit2')->nullable();
            $table->string('Quantity3')->nullable();
            $table->string('Unit3')->nullable();
            $table->string('Quantity4')->nullable();
            $table->string('Unit4')->nullable();
            $table->string('LocationName')->nullable();
            $table->string('Rate')->nullable();
            $table->string('DiscountPercentage')->nullable();
            $table->string('BeforeDiscountAmount')->nullable();
            $table->string('DiscountAmount')->nullable();
            $table->string('Amount')->nullable();
            $table->string('LineType')->nullable();
            $table->string('PlaceOfSupply')->nullable();
            $table->string('ReverceChargeApplicable')->nullable();
            $table->string('GSTINNo')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('flipcartsheets');
    }
}
