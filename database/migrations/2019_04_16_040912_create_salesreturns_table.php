<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSalesreturnsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('salesreturns', function (Blueprint $table) {
            $table->increments('id');
            $table->string('Vch_Series')->nullable();
            $table->string('Bill_Date')->nullable();
            $table->string('Bill_No')->nullable();
            $table->string('Party_Name')->nullable();
            $table->string('Item_Name')->nullable();
            $table->string('Qty')->nullable();
            $table->string('Price')->nullable();
            $table->string('Amount')->nullable();
            $table->string('Amount_Grand_Total')->nullable();
            $table->string('Q1')->nullable();     
            $table->string('P1')->nullable();
            $table->string('A1')->nullable();
            $table->string('AMT_G1')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('salesreturns');
    }
}
