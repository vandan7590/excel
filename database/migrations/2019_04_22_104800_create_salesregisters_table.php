<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSalesregistersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('salesregisters', function (Blueprint $table) {
            $table->increments('id');
            $table->string('Vch_Series')->nullable();
            $table->string('Bill_Date')->nullable();
            $table->string('Bill_No')->nullable();
            $table->string('Party_Name')->nullable();
            $table->string('Item_Name')->nullable();
            $table->string('Qty')->nullable();
            $table->string('Price')->nullable();
            $table->string('Amount')->nullable();
            $table->string('Cash_Acc_Amount')->nullable();
            $table->string('Credit_Card2_Amount')->nullable();
            $table->string('Credit_Card3_Amount')->nullable();
            $table->string('Amount_Grand_Total')->nullable();
            $table->string('QTYS')->nullable();     
            $table->string('PRICES')->nullable();
            $table->string('AMOUNTS')->nullable();
            $table->string('Cash_Acc')->nullable();
            $table->string('CC2')->nullable();
            $table->string('CC3')->nullable();
            $table->string('GTOTAL')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('salesregisters');
    }
}
