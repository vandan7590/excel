<!DOCTYPE html>
<html lang="en">
@include('front.layouts.design')
@if($errors->any())
@include('errors.errors') 
@endif
<body>
  <h1 style="margin-left:25px;">Sales Register</h1><br>
  <div class="container">
      <div class="row">
        <div class="col-sm-12">
            <form action="{{route('salesregister')}}" method="post" enctype="multipart/form-data">
              {{csrf_field()}}
              <div class="row form-group">                
                <div class="col-md-12">
                    <input type="file" name="import_file" accept=".xls, .xlsx" /><br>
                </div>
              </div><br>
              <label> Amount Deduct: </label>
              <input type="text" name="deduct" style="width:45px; "> %<br><br>
              <button type="submit" class="btn btn-info"><li class="fa fa-download"> Export </li> </button>
            </form><br>
        </div>
      </div>
  </div>      
</body>