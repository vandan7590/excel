<!DOCTYPE html>
<html lang="en">
@include('front.layouts.design')
@if($errors->any())
@include('errors.errors') 
@endif
<div class="wrapper">        
<a href="{{route('logout')}}" class="btn btn-primary logout">Logout</a>
<section class="content">
    <div class="row">
        <div class="col-md-6">
            <div class="box box-primary">
                <h3>Sales Register</h3><br>
                <form action="{{route('salesregister')}}" method="post" enctype="multipart/form-data">
                    {{csrf_field()}}
                    <div class="form-group">                
                        <div class="col">
                            <input type="file" name="import_file" accept=".xls, .xlsx" /><br>
                        </div>
                    </div><br>
                    <label> Amount Deduct: </label>
                    <input type="text" name="deduct" style="width:45px; "> %<br><br>
                    <button type="submit" class="btn btn-info"><li class="fa fa-download"> Convert </li> </button>
                </form>
            </div>
        </div>
        <div class="col-md-6">
            <div class="box box-primary">
                <h3>Sales Return</h3><br>
                <form action="{{route('salesreturn')}}" method="post" enctype="multipart/form-data">
                    {{csrf_field()}}
                    <div class="form-group">                
                        <div class="col">
                            <input type="file" name="import_file" accept=".xls, .xlsx" /><br>
                        </div>
                    </div><br>                        
                    <label> Amount Deduct: </label>
                    <input type="text" name="deduct" style="width:45px; "> %<br><br>
                    <button type="submit" class="btn btn-info"><li class="fa fa-download"> Convert </li> </button>                
                </form>
            </div>
        </div>
    </div> 
</section>
</div>
