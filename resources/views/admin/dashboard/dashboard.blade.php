@extends('admin.layouts.app')
@section('content')
    <section class="content-header">
        <h1>
            Import Export Form
        </h1>
        <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
            <li class="active">Dashboard</li>
        </ol>
    </section>
        <!-- Main content -->
    <section class="content">
        <div class="row">
            <div class="col-md-6">
                <div class="box-body">
                <form action="{{route('import')}}" method="post" enctype="multipart/form-data">
                    {{csrf_field()}}
                    <div class="hero-content">                                
                        <input type="file" name="import_file" class="btn btn-warning" accept=".xls, .xlsx" /><br>
                    </div>                        
                    <br>
                    <label> Amount Deduct: </label>
                    <input type="text" name="deduct" style="width:45px; "> %<br><br>
                    
                    <input type="submit" class="btn btn-primary m-btn m-btn--icon m-btn--pill m-btn--air" value="Export Data">
                    <span> <i class="la la-cloud-download"></i></span>                
                </form>
                </div>     
            </div>
        </div>                 
    </section><!-- /.content -->   
    
@endsection
