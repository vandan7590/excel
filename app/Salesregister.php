<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Salesregister extends Model
{
    protected $fillable = [
        'Vch_Series','Bill_Date','Bill_No','Party_Name','Item_Name','Qty','Price','Amount','Cash_Acc_Amount',
        'Credit_Card2_Amount','Credit_Card3_Amount','Amount_Grand_Total','QTYS','PRICES','AMOUNTS','Cash_Acc',
        'CC2','CC3','GTOTAL'
    ];

    public $timestamps = false;
}
