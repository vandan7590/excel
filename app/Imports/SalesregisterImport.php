<?php

namespace App\Imports;
use App\Salesregister;
use App\Http\Requests\DateRequest;
use Maatwebsite\Excel\Concerns\ToModel;
use Maatwebsite\Excel\Concerns\WithBatchInserts;
use Maatwebsite\Excel\Concerns\WithChunkReading;


class SalesregisterImport implements ToModel, WithChunkReading, WithBatchInserts
{
    /**
    * @param array $row
    *
    * @return \Illuminate\Database\Eloquent\Model|null
    */
    public function model(array $row)
    {        
        return new Salesregister([
            'Vch_Series' => $row[0],            
            'Bill_Date' => \PhpOffice\PhpSpreadsheet\Shared\Date::excelToDateTimeObject($row['1'])->format('d-m-y'),
            'Bill_No' => $row[2],               'Party_Name' => $row[3], 
            'Item_Name' => $row[4],             'Qty' => $row[5],
            'Price' => $row[6],                 'Amount' => $row[7], 
            'Cash_Acc_Amount' => $row[8],       'Credit_Card2_Amount' => $row[9], 
            'Credit_Card3_Amount' => $row[10],  'Amount_Grand_Total' => $row[11], 
           ]);
    }
    
    public function chunkSize(): int
    {
        return 1000;
    }
    
    public function batchSize(): int
    {
        return 1000;
    }
}

