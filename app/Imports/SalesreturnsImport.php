<?php

namespace App\Imports;

use App\Salesreturn;
use Maatwebsite\Excel\Concerns\ToModel;
use Maatwebsite\Excel\Concerns\WithBatchInserts;
use Maatwebsite\Excel\Concerns\WithChunkReading;

class SalesreturnsImport implements ToModel, WithChunkReading, WithBatchInserts
{
    /**
    * @param array $row
    *
    * @return \Illuminate\Database\Eloquent\Model|null
    */
    public function model(array $row)
    {
        return new Salesreturn([
            'Vch_Series' => $row[0],            
            'Bill_Date' => \PhpOffice\PhpSpreadsheet\Shared\Date::excelToDateTimeObject($row['1'])->format('d-m-y'),
            'Bill_No' => $row[2],               'Party_Name' => $row[3], 
            'Item_Name' => $row[4],             'Qty' => $row[5],
            'Price' => $row[6],                 'Amount' => $row[7], 
            'Amount_Grand_Total' => $row[8], 
        ]);
    }
    
    public function chunkSize(): int
    {
        return 1000;
    }
    
    public function batchSize(): int
    {
        return 1000;
    }
}
