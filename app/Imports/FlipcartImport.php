<?php

namespace App\Imports;

use App\Flipcartsheet;
use Maatwebsite\Excel\Concerns\ToModel;
use Maatwebsite\Excel\Concerns\WithBatchInserts;
use Maatwebsite\Excel\Concerns\WithChunkReading;

class FlipcartImport implements ToModel, WithChunkReading, WithBatchInserts
{
    /**
    * @param array $row
    *
    * @return \Illuminate\Database\Eloquent\Model|null
    */
    public function model(array $row)
    {
        return new Flipcartsheet([
           'BillNo' => $row[0],                     'BillDate' => $row[1],
           'BillType' => $row[2],                   'PartyName' => $row[3],
           'InvoiceType' => $row[4],                'BillAmount' => $row[5],
           'City' => $row[6],                       'state' => $row[7],
           'BillToBillRequired' => $row[8],         'BookCode' => $row[9],
           'Narration' => $row[10],                 'ProductName' => $row[11],
           'Quantity' => $row[12],                  'Unit' => $row[13],
           'Quantity2' => $row[14],                 'Unit2' => $row[15],
           'Quantity3' => $row[16],                 'Unit3' => $row[17],
           'Quantity4' => $row[18],                 'Unit4' => $row[19],
           'LocationName' => $row[20],              'Rate' => $row[21],
           'DiscountPercentage' => $row[22],        'BeforeDiscountAmount' => $row[23],
           'DiscountAmount' => $row[24],            'Amount' => $row[25],
           'LineType' => $row[26],                  'PlaceOfSupply' => $row[27],
           'ReverceChargeApplicable' => $row[28],   'GSTINNo' => $row[29]
        ]);
    }

    public function chunkSize(): int
    {
        return 1000;
    }
    
    public function batchSize(): int
    {
        return 1000;
    }
}
