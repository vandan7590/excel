<?php

namespace App\Http\Controllers;
use App\Imports\SalesregisterImport;
use App\Exports\SalesregisterExport;
use App\Http\Requests\ExcelRequest;
use Maatwebsite\Excel\Facades\Excel;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
use App\salesregister;

class ExcelController extends Controller
{
    public function sales()
    {
        return view('front.main');
    }

    public function import(ExcelRequest $request) 
    {              
        Excel::import(new SalesregisterImport, $request->file('import_file'));        
        
        $deduct=$request->get('deduct');       
        $all_imports = Salesregister::all();        
        $value="";

        foreach($all_imports as $import)
        {                         
            if($import->Bill_No){
                if($import->Cash_Acc_Amount > 0){
                    $value="cash";
                }elseif($import->Credit_Card2_Amount > 0){
                    $value="credit";
                }
            } 
            $value = $value;
            if($value == "cash")
            {
                $total=str_replace(',','',$import->Price);
                $import->update([
                        'QTYS' => $import->Qty,
                        'PRICES' => $total * $deduct/100,
                        'AMOUNTS' => $import->Amount * $deduct /100,
                        'Cash_Acc' => $import->Cash_Acc_Amount * $deduct/100,
                        'GTOTAL' => $import->Cash_Acc_Amount * $deduct/100]);   
            }                   
            elseif($value == "credit")
            {                
                $import->update([
                    'QTYS' => $import->Qty,
                    'PRICES' => $import->Price,
                    'AMOUNTS' => $import->Amount,
                    'Cash_Acc' => $import->Credit_Card2_Amount,
                    'GTOTAL' => $import->Credit_Card2_Amount]);    
            }                                
        }         

        $file = Excel::download(new SalesregisterExport,'Sales Register Cr.xlsx');        

        Salesregister::truncate();
        return $file;
    }
}
