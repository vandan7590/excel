<?php

namespace App\Http\Controllers;
use Illuminate\Http\Request;
use App\Imports\SalesreturnsImport;
use App\Exports\SalesreturnsExport;
use App\Http\Requests\ExcelRequest;
use Maatwebsite\Excel\Facades\Excel;
use App\Salesreturn;

class SalesreturnController extends Controller
{
    public function salesreturn()
    {
        return view('front.layouts.salesreturn');
    }

    public function import(ExcelRequest $request) 
    {              
        Excel::import(new SalesreturnsImport, $request->file('import_file'));        
        
        $deduct=$request->get('deduct');       
        $all_imports = Salesreturn::all();        

        foreach($all_imports as $import)
        {     
            $total=str_replace(',','',$import->Price);

            $import->update([
                'Q1' => $import->Qty,
                'P1' => $total * $deduct/100,
                'A1' => $import->Amount * $deduct /100,
                'AMT_G1' => $import->Amount_Grand_Total * $deduct/100]);                                
        }         

        $file = Excel::download(new SalesreturnsExport, 'Sales Return2019 (Cr.xlsx');    
        Salesreturn::truncate();
        return $file;
    }
}
