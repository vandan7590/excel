<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests\LoginRequest;
use App\User;
use Auth;

class LoginController extends Controller
{
    public function login()
    {
        return view('login.login');
    }

    public function loginstore(LoginRequest $request)
    {
        if(User::loginUser($request->email,$request->password))
        {
            $email=$request->get('email');
            $role=User::where('email','=',$email)->pluck('role')->first();
            if($role=='1')
            {
                $name=User::where('email','=',$email)->pluck('name')->first();
                return \Redirect::route('front',['name'=>$name]);
            }
            else
            {
                return redirect()->back();
            }
        }
        else
        {
            return redirect()->back();            
        }
    }

    public function logout(Request $request)
    {                         
        \Auth::logout();                 
        return \Redirect::route('login');        
        
    }
}

