<?php

namespace App\Http\Controllers;

use App\Exports\FlipcartExport;
use App\Flipcartsheet;
use Illuminate\Http\Request;
use App\Imports\FlipcartImport;
use Maatwebsite\Excel\Facades\Excel;

class ConverterController extends Controller
{
    public function view()
    {
        return view('front.converter');
    }

    public function import(Request $request) 
    {
        Excel::import(new FlipcartImport, $request->file('import_file'));

        $file = Excel::download(new FlipcartExport,'flipcartsheet.xlsx');
        
        Flipcartsheet::truncate();
        return $file;
    }
}
