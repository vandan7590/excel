<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;

class DashboardController extends Controller
{
    public function __construct(){
        $this->middleware('auth');
    }

    public function front($name)
    {
        // return view('front.front');
        $data=User::where('name','=',$name)->get();
        return view('front.front',compact('data','name'));
    }
}
