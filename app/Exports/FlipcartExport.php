<?php

namespace App\Exports;

use App\Flipcartsheet;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\Exportable;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Illuminate\Contracts\Support\Responsable;

class FlipcartExport implements FromCollection, WithHeadings, Responsable
{
    /**
    * @return \Illuminate\Support\Collection
    */

    use Exportable;

    public function collection()
    {
        return $flipcart= Flipcartsheet::select(
            'BillNo',           'BillDate',             'BillType',
            'PartyName',        'InvoiceType',          'BillAmount',
            'City',             'state',                'BillToBillRequired',
            'BookCode',         'Narration',            'ProductName',
            'Quantity',         'Unit',                 'Quantity2',
            'Unit2',            'Quantity3',            'Unit3',
            'Quantity4',        'Unit4',                'LocationName',
            'Rate',             'DiscountPercentage',   'BeforeDiscountAmount',
            'DiscountAmount',   'Amount',               'LineType',
            'PlaceOfSupply',                            'ReverceChargeApplicable',
            'GSTINNo'
        )->where('id', '>=' ,1)->get();
    }

    public function headings(): array
    {
        return [
            'BIllNo',           'BillDate',             'BillType',
            'PartyName',        'InoviceType',          'BillAmount',
            'City',             'state',                'BillToBillRequired',
            'BookCode',         'Narration',            'ProductName',
            'Quantity',         'Unit',                 'Quantity2',
            'Unit2',            'Quantity3',            'Unit3',
            'Quantity4',        'Unit4',                'LocationName',
            'Rate',             'DiscountPercentage',   'BeforeDiscountAmount',
            'DiscountAmount',   'Amount',               'LineType',
            'PlaceOfSupply',                            'ReverceChargeApplicable',
            'GSTINNo'
        ];
    }
}
