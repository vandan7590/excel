<?php

namespace App\Exports;

use App\Salesregister;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\Exportable;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Illuminate\Contracts\Support\Responsable;


class SalesregisterExport implements FromCollection, WithHeadings, Responsable
{
    /**
    * @return \Illuminate\Support\Collection
    */
    use Exportable;

    public function collection()
    {       
        // $users=User::all();
         return $users= Salesregister::select(
            'Vch_Series',   'Bill_Date',    "Bill_No",
            'Party_Name',   'Item_Name',    'Qty',
            'Price',        'Amount',       'Cash_Acc_Amount',
            'Credit_Card2_Amount',          'Credit_Card3_Amount',
            'Amount_Grand_Total',           'QTYS',
            'PRICES',       'AMOUNTS',      'Cash_Acc',
            'CC2',          'CC3',          'GTOTAL'
        )->where('id', '>=' ,7)->get();
    }

    public function headings(): array
    {
        return [
            'Vch_Series',   'Bill_Date',    "Bill_No",
            'Party_Name',   'Item_Name',    'Qty',
            'Price',        'Amount',       'Cash_Acc_Amount',
            'Credit_Card2_Amount',          'Credit_Card3_Amount',
            'Amount_Grand_Total',           'QTYS',
            'PRICES',       'AMOUNTS',      'Cash_Acc',
            'CC2',          'CC3',          'GTOTAL',
        ];
    }
}
