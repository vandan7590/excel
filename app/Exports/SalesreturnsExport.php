<?php

namespace App\Exports;

use App\Salesreturn;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\Exportable;
use Maatwebsite\Excel\Concerns\WithHeadings;

class SalesreturnsExport implements FromCollection, WithHeadings
{
    /**
    * @return \Illuminate\Support\Collection
    */
    public function collection()
    {
        return $users= Salesreturn::select(
            'Vch_Series',   'Bill_Date',    "Bill_No",
            'Party_Name',   'Item_Name',    'Qty',
            'Price',        'Amount', 
            'Amount_Grand_Total',           'Q1',           
            'P1',           'A1',           'AMT_G1'
        )->where('id', '>=' ,7)->get();
    }

    public function headings(): array
    {
        return [
            'Vch_Series',   'Bill_Date',    "Bill_No",
            'Party_Name',   'Item_Name',    'Qty',
            'Price',        'Amount',       'Amount_Grand_Total',           
            'Q1',           'P1',           'A1',      
            'AMT_G1',
        ];
    }
}
