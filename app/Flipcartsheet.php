<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Flipcartsheet extends Model
{
    protected $fillable = [
        'BillNo','BillDate','BillType','PartyName','InvoiceType','BillAmount','City','state','BillToBillRequired',
        'BookCode','Narration','ProductName','Quantity','Unit','Quantity2','unit2','Quantity3','unit3','Quantity4','unit4',
        'LocationName','Rate','DiscountPercentage','BeforeDiscountAmount','DiscountAmount','Amount','LineType','PlaceOfSupply',
        'ReverceChargeApplicable','GSTINNo'
    ];

    public $timestamps = false;
}
