<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Salesreturn extends Model
{
    protected $fillable = [
        'Vch_Series','Bill_Date','Bill_No','Party_Name','Item_Name','Qty','Price','Amount','Amount_Grand_Total',
        'Q1','P1','A1','AMT_G1'
    ];

    public $timestamps = false;
}
